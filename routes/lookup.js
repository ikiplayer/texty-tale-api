var express = require("express");
const TagController = require("../controllers/TagController");

var router = express.Router();

router.get("/tag", TagController.tagList);

module.exports = router;
