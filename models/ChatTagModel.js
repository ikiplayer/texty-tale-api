const mongoose = require("mongoose");
var Schema = mongoose.Schema;

// USER CAN SEE ALL THEIR CREATED CHATS

const ChatTagSchema = new mongoose.Schema(
  {
    tag: { type: Schema.Types.ObjectId, ref: "Tag" },
    chat: { type: Schema.Types.ObjectId, ref: "Chat" },
    updated_at: { type: Date },
    created_at: { type: Date },
    updated_by: { type: String },
    created_by: { type: String },
  },
  { timestamps: true }
);

const ChatTag = mongoose.model("ChatTag", ChatTagSchema);

module.exports = ChatTag;
