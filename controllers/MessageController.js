const Chat = require("../models/ChatModel");
const ChatTag = require("../models/ChatTagModel");
const Message = require("../models/MessageModel");
const ChatFakeUser = require("../models/ChatFakeUserModel");
const FakeUser = require("../models/FakeUserModel");
const Tag = require("../models/TagModel");
const User = require("../models/UserModel");
const { body, validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
var mongoose = require("mongoose");
const upload = require("../helpers/upload");
mongoose.set("useFindAndModify", false);

exports.messageImageUpload = [
  upload,
  async (req, res) => {
    if (req.file == undefined) {
      return apiResponse.validationErrorWithData(res, "No file found");
    }

    try {
      var newMessage = new Message({});
    } catch (error) {}
  },
];
