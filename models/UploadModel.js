var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var UploadSchema = new Schema(
  {
    filename: { type: String, required: true },
    mime_type: { type: String, required: true },
    original_name: { type: String, required: true },
    type: { type: String, required: true },
    is_used: { type: Boolean, required: true, default: false },
    user: { type: Schema.ObjectId, ref: "User", required: false },
    is_nfsw: { type: Boolean, required: false, default: false },
  },
  {
    timestamps: true,
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
  }
);

UploadSchema.virtual("url").get(function () {
  if (this.type == "chat") {
    return process.env.UPLOAD_PATH + "uploads/" + this.filename;
  } else if (this.type == "chat_gif") {
    return process.env.UPLOAD_PATH + "chat-gif/" + this.filename;
  } else {
    return process.env.UPLOAD_PATH + "uploads/" + this.filename;
  }
});

module.exports = mongoose.model("Upload", UploadSchema);
