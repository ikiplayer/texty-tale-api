var express = require("express");
var authRouter = require("./auth");
var bookRouter = require("./book");
var userRouter = require("./book");
var chatRouter = require("./chat");
var fakeUserRouter = require("./fakeUser");
var tagRouter = require("./tag");
var chatLikeRouter = require("./chatLike");
var uploadRouter = require("./upload");
var lookupRouter = require("./lookup");

var app = express();

app.use("/auth/", authRouter);
app.use("/user/", userRouter);
app.use("/book/", bookRouter);
app.use("/chat/", chatRouter);
app.use("/fakeuser/", fakeUserRouter);
app.use("/tag/", tagRouter);
app.use("/upload/", uploadRouter);
app.use("/chat-like/", chatLikeRouter);
app.use("/lookup/", lookupRouter);

module.exports = app;
