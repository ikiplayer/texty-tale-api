var express = require("express");
const UserChatLikeController = require("../controllers/UserChatLikeController");

var router = express.Router();

router.get("/", UserChatLikeController.userChatLikeList);
// router.get("/:id", UserChatLikeController.bookDetail);
// router.post("/", UserChatLikeController.bookStore);
router.put("/:id", UserChatLikeController.userChatLikeUpdate);
// router.delete("/:id", UserChatLikeController.bookDelete);

module.exports = router;
