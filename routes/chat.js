var express = require("express");
const ChatController = require("../controllers/ChatController");

var router = express.Router();

// router.get("/my-chats", ChatController.userChatList);
router.get("/", ChatController.chatList);
router.get("/puppeteer/:id", ChatController.puppeteerChatDetailAccess);
router.get("/:id", ChatController.chatDetail);
router.post("/", ChatController.chatStore);
router.put("/:id", ChatController.chatUpdate);
router.delete("/:id", ChatController.chatDelete);

module.exports = router;
