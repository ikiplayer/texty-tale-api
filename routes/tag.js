var express = require("express");
const TagController = require("../controllers/TagController");

var router = express.Router();

router.get("/", TagController.tagList);
router.post("/", TagController.tagStore);

module.exports = router;
