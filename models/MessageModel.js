const mongoose = require("mongoose");
var Schema = mongoose.Schema;

const MessageSchema = new mongoose.Schema(
  {
    message: { type: String },
    image: { type: Schema.Types.ObjectId, ref: "Upload" },
    order: { type: Number },
    datetime: { type: Date },
    fake_user: { type: Schema.Types.ObjectId, ref: "FakeUser" },
    chat: { type: Schema.Types.ObjectId, ref: "Chat" },
    message_type: { type: Number },
    updated_at: { type: Date },
    created_at: { type: Date },
    updated_by: { type: String },
    created_by: { type: String },
  },
  { timestamps: true }
);

// CHAT TYPE
// - RIGHT_BUBBLE= 0;
// - LEFT_BUBBLE = 1;
// - MIDDLE_BUBBLE = 2;
// - MIDDLE_CHAT_TYPE = 3;

const Message = mongoose.model("Message", MessageSchema);

module.exports = Message;
