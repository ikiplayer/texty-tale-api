var express = require("express");
const UploadController = require("../controllers/UploadController");

var router = express.Router();

router.post("/", UploadController.uploadFileSave);

module.exports = router;
