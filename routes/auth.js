var express = require("express");
const AuthController = require("../controllers/AuthController");

var router = express.Router();

router.post("/register", AuthController.register);
router.post("/login", AuthController.login);
router.post("/verify-otp", AuthController.verifyConfirm);
router.post("/resend-verify-otp", AuthController.resendConfirmOtp);
router.post("/verify-email", AuthController.verifyEmail);
router.post("/fb", AuthController.faceboookAuth);
router.post("/reset-password-link", AuthController.resetPasswordLink);
router.post("/reset-password", AuthController.updatePasswordWithOTP);

// router.get("/google/redirect", passport.authenticate("google"), (req, res) => {
//   res.send(req.user);
// });

module.exports = router;
