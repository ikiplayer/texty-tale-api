require("dotenv").config();
const puppeteer = require("puppeteer");
const GIFEncoder = require("gif-encoder");
const fs = require("fs");
const getPixels = require("get-pixels");
const tempDir = "./public/gif-temp/";
const workDir = "./public/chat-gif/";
const Upload = require("../models/UploadModel");
const Chat = require("../models/ChatModel");
const { PendingXHR } = require("pending-xhr-puppeteer");

if (!fs.existsSync(tempDir)) {
  fs.mkdirSync(tempDir);
}

async function run(req) {
  const viewPortSize = { width: 400, height: 300 };
  const encoder = new GIFEncoder(viewPortSize.width, viewPortSize.height);
  let fileNameGif = workDir + req.id + ".gif";
  let fileNameStaticGifImage = workDir + req.id + ".png";
  let file = fs.createWriteStream(fileNameGif);

  // Setup gif encoder parameters
  encoder.setFrameRate(60);
  encoder.pipe(file);
  encoder.setQuality(80);
  encoder.setDelay(1000);
  encoder.writeHeader();
  encoder.setRepeat(0);

  const browser = await puppeteer.launch({
    headless: true,
    defaultViewport: {
      width: viewPortSize.width,
      height: viewPortSize.height,
      isMobile: false,
    },
  });
  const page = await browser.newPage();
  //   await page.setViewport(viewPortSize);
  //   await page.emulate({
  //     viewport: {
  //       width: viewPortSize.width,
  //       height: viewPortSize.height,
  //       isMobile: true,
  //     },
  //   });
  let url = `${process.env.CLIENT_URL}puppeteer/chat/${req.chat_type}/${req.id}?access=${process.env.PUPPETEER_SECRET}`;
  console.log("puppeteer url", url);
  await page.goto(url, {
    waitUntil: "networkidle2",
  });

  // await Promise.race([page.waitForNavigation({ waitUntil: "networkidle0" }), page.waitForSelector(".Error")]);

  //   const scrollHeight = await page.evaluate(async () => {
  //     return document.body.scrollHeight;
  //   });
  //   console.log(scrollHeight);

  let listOfPNGs = [];
  //   let maxScreenShot = Math.ceil(scrollHeight / viewPortSize.height);
  //   console.log(maxScreenShot);

  for (let i = 0; i < 10; i++) {
    let filename = tempDir + req.id + "_" + i + ".png";
    listOfPNGs.push(filename);
    await page.screenshot({ path: filename });
    if (i == 0) {
      await page.screenshot({ path: fileNameStaticGifImage });
    }
    await scrollPage();
  }

  //   let listOfPNGs = fs
  //     .readdirSync(tempDir)
  //     .map((a) => a.substr(0, a.length - 4) + "")
  //     .sort(function (a, b) {
  //       return a - b;
  //     })
  //     .map((a) => tempDir + a.substr(0, a.length) + ".png");

  console.log(listOfPNGs);

  addToGif(listOfPNGs);

  function addToGif(images, counter = 0) {
    getPixels(images[counter], function (err, pixels) {
      if (pixels && pixels.data) {
        encoder.addFrame(pixels.data);
        encoder.read();
        if (counter === images.length - 1) {
          encoder.finish();
          browser.close();
          saveData();
          cleanUp(images, function (err) {
            if (err) {
              console.log(err);
              if (this.itemSelected) {
                this.chat = this.itemSelected;
              }
            } else {
              console.log("Gif created!");
            }
          });
        } else {
          addToGif(images, ++counter);
        }
      }
    });
  }

  function cleanUp(listOfPNGs, callback) {
    let i = listOfPNGs.length;
    listOfPNGs.forEach(function (filepath) {
      fs.unlink(filepath, function (err) {
        i--;
        if (err) {
          callback(err);
          return;
        } else if (i <= 0) {
          callback(null);
        }
      });
    });
  }

  async function saveData() {
    console.log("save data");

    var upload = new Upload({
      filename: req.id + ".gif",
      mime_type: "image/gif",
      original_name: req.id + ".gif",
      type: "chat_gif",
      is_used: true,
      // user: req.user._id,
    });

    console.log(upload);

    upload.save(function (err, uploadedData) {
      if (err) {
        console.log(err);
        return;
      }

      console.log("uploaded data", uploadedData);

      Chat.findByIdAndUpdate(
        req.id,
        {
          main_image: uploadedData.id,
        },
        function (err) {
          if (err) {
            console.log(err);
            return;
          }
        }
      );
    });
  }

  async function scrollPage() {
    await page.evaluate(async () => {
      window.scrollBy(0, 75);
    });
  }
}

module.exports.run = run;
