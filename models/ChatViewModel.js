const mongoose = require("mongoose");
var Schema = mongoose.Schema;
// USER CAN SEE ALL THEIR CREATED CHATS

const ChatViewSchema = new mongoose.Schema(
  {
    chat: { type: Schema.Types.ObjectId, ref: "Chat", required: true },
    ip_address: { type: String, required: true },
    updated_at: { type: Date },
    created_at: { type: Date },
    updated_by: { type: String },
    created_by: { type: String },
  },
  { timestamps: true }
);

const ChatView = mongoose.model("ChatView", ChatViewSchema);

module.exports = ChatView;
