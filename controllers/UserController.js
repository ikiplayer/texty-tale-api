const User = require("../models/UserModel");
const { body, validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
var mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);

// User Schema
function UserData(data) {
  this.id = data._id;
  this.username = data.username;
  this.email = data.email;
  this.image_url = data.image_url;
  this.status = data.status;
  this.created_chats = data.created_chats;
  this.favorites = data.favorites;
  this.fake_users = data.fake_users;
  this.created_at = data.created_at;
  this.updated_at = data.updated_at;
}

// exports.userDetail = [
//   auth,
//   async (req, res) => {
//     if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
//       return apiResponse.successResponseWithData(res, "Operation success", {});
//     }
//     try {
//       Book.findOne(
//         { _id: req.params.id, user: req.user._id },
//         "_id title description isbn createdAt"
//       ).then((book) => {
//         if (book !== null) {
//           let bookData = new BookData(book);
//           return apiResponse.successResponseWithData(
//             res,
//             "Operation success",
//             bookData
//           );
//         } else {
//           return apiResponse.successResponseWithData(
//             res,
//             "Operation success",
//             {}
//           );
//         }
//       });
//     } catch (err) {
//       return apiResponse.ErrorResponse(res, err);
//     }
//   },
// ];

/**
 * User update.
 *
 * @param {string}      title
 * @param {string}      description
 * @param {string}      isbn
 *
 * @returns {Object}
 */
// exports.userUpdate = [
//   auth,
//   (req, res) => {
//     try {
//       const errors = validationResult(req);
//       var user = new User({
//         title: req.body.title,
//         description: req.body.description,
//         isbn: req.body.isbn,
//         _id: req.params.id,
//       });

//       if (!errors.isEmpty()) {
//         return apiResponse.validationErrorWithData(
//           res,
//           "Validation Error.",
//           errors.array()
//         );
//       } else {
//         if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
//           return apiResponse.validationErrorWithData(
//             res,
//             "Invalid Error.",
//             "Invalid ID"
//           );
//         } else {
//           User.findById(req.params.id, function (err, validUser) {
//             if (validUser === null) {
//               return apiResponse.notFoundResponse(
//                 res,
//                 "User not exists with this id"
//               );
//             } else {
//               //Check authorized user
//               if (validUser.user.toString() !== req.user._id) {
//                 return apiResponse.unauthorizedResponse(
//                   res,
//                   "You are not authorized to do this operation."
//                 );
//               } else {
//                 //update book.
//                 User.findByIdAndUpdate(req.params.id, user, {}, function (err) {
//                   if (err) {
//                     return apiResponse.ErrorResponse(res, err);
//                   }
//                 });
//               }
//             }
//           });
//         }
//       }
//     } catch (err) {
//       //throw error in json response with status 500.
//       return apiResponse.ErrorResponse(res, err);
//     }
//   },
// ];

// As a user, I can add favorite chat

exports.updateFavoriteChat = [
  auth,
  async (req, res) => {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return apiResponse.successResponseWithData(res, "Operation success", {});
    }

    try {
      let user = await User.findById(req.user._id);
      let chat = await Chat.findById(req.params._id);
      if (chat !== null && user !== null) {
        if (req.params.status === 0) {
          user.favorites = user.favorites.filter((item) => {
            return item != req.params._id;
          });
        } else if (req.params.status === 1) {
          user.favorites.push(chat);
        } else {
          return apiResponse.ErrorResponse(res, "Status is required");
        }

        await user.save();
        return apiResponse.successResponseWithData(
          res,
          "Operation success",
          user
        );
      } else {
        return apiResponse.successResponseWithData(
          res,
          "Operation success",
          {}
        );
      }
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

// As a user, I can remove favorite chat

exports.removeFavoriteChat = [
  auth,
  async (req, res) => {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return apiResponse.successResponseWithData(res, "Operation success", {});
    }

    try {
      let user = await User.findById(req.user._id);
      let chat = await Chat.findById(req.params._id);
      if (chat !== null) {
        user.favorites = user.favorites.filter((item) => {
          return item != req.params._id;
        });
        await user.save();
        return apiResponse.successResponseWithData(
          res,
          "Operation success",
          user
        );
      } else {
        return apiResponse.successResponseWithData(
          res,
          "Operation success",
          {}
        );
      }
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];
