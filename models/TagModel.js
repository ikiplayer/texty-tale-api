const mongoose = require("mongoose");
var Schema = mongoose.Schema;

const TagSchema = new mongoose.Schema(
  {
    name: { type: String, required: true },
    main_tag: { type: String, required: true, default: "" },
    updated_at: { type: Date },
    created_at: { type: Date },
    updated_by: { type: String },
    created_by: { type: String },
  },
  { timestamps: true }
);

const Tag = mongoose.model("Tag", TagSchema);

module.exports = Tag;
