const Tag = require("../models/TagModel");
const { body, validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
var mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);

/**
 * Tag List.
 *
 * @returns {Object}
 */
exports.tagList = [
  function (req, res) {
    try {
      Tag.find({}).then((tags) => {
        if (tags.length > 0) {
          return apiResponse.successResponseWithData(
            res,
            "Operation success",
            tags
          );
        } else {
          return apiResponse.successResponseWithData(
            res,
            "Operation success",
            []
          );
        }
      });
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Tag store.
 *
 *
 * @returns {Object}
 */
exports.tagStore = [
  body("name", "Name must not be empty.")
    .isLength({ min: 1 })
    .trim()
    .custom((value, { req }) => {
      return (
        Tag.findOne({ name: req.body.name }).then((tag) => {
          if (tag) {
            return Promise.reject("Tag already exist with this name");
          }
        }),
        body("main_tag", "Main Tag must not be empty.")
          .isLength({ min: 1 })
          .trim()
      );
    }),
  sanitizeBody("*").escape(),
  (req, res) => {
    try {
      const errors = validationResult(req);
      var tag = new Tag({
        name: req.body.name,
        main_tag: req.body.main_tag,
      });

      if (!errors.isEmpty()) {
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      } else {
        tag.save(function (err, newTag) {
          if (err) {
            return apiResponse.ErrorResponse(res, err);
          }
          return apiResponse.successResponseWithData(
            res,
            "Tag add Success.",
            newTag
          );
        });
      }
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];