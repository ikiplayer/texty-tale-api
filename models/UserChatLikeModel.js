var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var UserChatLikeSchema = new mongoose.Schema(
  {
    chat: { type: Schema.Types.ObjectId, ref: "Chat" },
    user: { type: Schema.Types.ObjectId, ref: "User" },
    is_liked: { type: Boolean, required: true, default: 1 },
    updated_at: Date,
    created_at: Date,
  },
  { timestamps: true }
);

// UserSchema.methods.gravatar = function gravatar(size) {
//   if (!size) {
//     size = 200;
//   }
//   if (!this.email) {
//     return `https://gravatar.com/avatar/?s=${size}&d=retro`;
//   }
//   const md5 = crypto.createHash("md5").update(this.email).digest("hex");
//   return `https://gravatar.com/avatar/${md5}?s=${size}&d=retro`;
// };

module.exports = mongoose.model("UserChatLike", UserChatLikeSchema);
