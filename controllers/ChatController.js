const Chat = require("../models/ChatModel");
const ChatView = require("../models/ChatViewModel");
const Message = require("../models/MessageModel");
const User = require("../models/UserModel");
const { body, validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
const authBypass = require("../middlewares/jwtBypass");
var mongoose = require("mongoose");
var puppeteer = require("../helpers/puppeteerGif");
const FakeUser = require("../models/FakeUserModel");
const Tag = require("../models/TagModel");
const ObjectId = mongoose.Types.ObjectId;
mongoose.set("useFindAndModify", false);
let Filter = require("bad-words");
let filter = new Filter({ replaceRegex: /[aeiouy0-9]/g, placeHolder: "*" });

/**
 * User Created Chats List.
 *
 * @returns {Object}
 */

function UploadData(req, data) {
  this.id = data._id;
  this.filename = data.filename;
  this.mime_type = data.mime_type;
  this.original_name = data.original_name;
  this.type = data.type;
  this.user = data.user;
  this.is_used = data.is_used;
  this.url = `${req.protocol}://${req.headers.host}/uploads/${data.filename}`;
}

exports.chatList = [
  authBypass,
  async (req, res) => {
    try {
      let mainTagQuery = req.query.main_tag;
      let searchQuery = req.query.search;
      let tagQuery = req.query.tag;
      let viewTypeQuery = req.query.view_type;
      let isRandom = req.query.is_random;
      let skip = req.query.skip ? req.query.skip : 0;
      let maxPerPage = req.query.max_per_page ? req.query.max_per_page : 20;
      let monthlyDate = new Date();
      monthlyDate.setDate(monthlyDate.getDate() - 30);
      let yearlyDate = new Date();
      yearlyDate.setDate(yearlyDate.getDate() - 365);

      let aggregateQuery = [
        {
          $match: {
            is_private: false,
          },
        },
        {
          $lookup: {
            from: "messages",
            localField: "messages",
            foreignField: "_id",
            as: "messages",
          },
        },
        {
          $lookup: {
            from: "chatviews",
            localField: "_id",
            foreignField: "chat",
            as: "views",
          },
        },
        {
          $lookup: {
            from: "userchatlikes",
            localField: "_id",
            foreignField: "chat",
            as: "likes",
          },
        },
        {
          $lookup: {
            from: "uploads",
            localField: "main_image",
            foreignField: "_id",
            as: "main_image",
          },
        },
        {
          $addFields: {
            total_views: { $size: "$views" },
            total_likes: {
              $size: {
                $filter: {
                  input: "$likes",
                  as: "likes",
                  cond: { $eq: ["$$likes.is_liked", true] },
                },
              },
            },
          },
        },
        {
          $addFields: {
            daily_views: {
              $size: {
                $filter: {
                  input: "$views",
                  as: "views",
                  cond: { $gte: ["$$views.createdAt", new Date()] },
                },
              },
            },
          },
        },
        {
          $addFields: {
            monthly_views: {
              $size: {
                $filter: {
                  input: "$views",
                  as: "views",
                  cond: { $gte: ["$$views.createdAt", monthlyDate] },
                },
              },
            },
          },
        },
        {
          $addFields: {
            yearly_views: {
              $size: {
                $filter: {
                  input: "$views",
                  as: "views",
                  cond: { $gte: ["$$views.createdAt", yearlyDate] },
                },
              },
            },
          },
        },
        {
          $addFields: {
            is_liked: {
              // $size: {
              $filter: {
                input: "$likes",
                as: "likes",
                cond: [{ $eq: ["$$likes.user", "$user"] }, []],
              },
              // }
            },
          },
        },
        {
          $unwind: {
            path: "$is_liked",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $unwind: {
            path: "$main_image",
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $addFields: {
            is_liked: {
              $cond: {
                if: { $eq: ["$is_liked.is_liked", true] },
                then: true,
                else: false,
              },
            },
          },
        },
        {
          $unset: ["views", "likes", "fake_users"],
        },
      ];

      if (viewTypeQuery != "my_chats") {
        aggregateQuery.unshift({
          $match: {
            is_private: false,
          },
        });
      }

      // VIEW TYPE
      switch (viewTypeQuery) {
        case "daily":
          aggregateQuery.push({
            $sort: {
              daily_views: -1,
            },
          });
          break;
        case "weekly":
          aggregateQuery.push({
            $sort: {
              weekly_views: -1,
            },
          });
          break;
        case "monthly":
          aggregateQuery.push({
            $sort: {
              monthly_views: -1,
            },
          });
          break;
        case "yearly":
          aggregateQuery.push({
            $sort: {
              yearly_views: -1,
            },
          });
          break;
        case "recent":
          aggregateQuery.push({
            $sort: {
              createdAt: -1,
            },
          });
          break;
        case "my_likes":
          aggregateQuery.push({
            $match: {
              is_liked: true,
            },
          });
          break;
        case "my_chats":
          aggregateQuery.push({
            $match: {
              user: ObjectId(`${req.user._id}`),
            },
          });
          aggregateQuery.shift();
          break;
        case "random":
          aggregateQuery.push({
            $sample: { size: Number(maxPerPage) },
          });
          break;
        default:
          break;
      }

      // TEST

      // TAG FILTER

      if (mainTagQuery) {
        let tags = [];
        tags = await Tag.find({ main_tag: mainTagQuery }, { _id: 1 });

        tags = tags.map((tag) => {
          return ObjectId(tag._id);
        });
        aggregateQuery.push({
          $match: {
            tags: { $in: tags },
          },
        });
      }

      if (searchQuery) {
        aggregateQuery.unshift({
          $sort: { score: { $meta: "textScore" } },
        });
        aggregateQuery.unshift({
          $match: { $text: { $search: searchQuery } },
        });
      }
      // const util = require("util");
      // console.log(
      //   util.inspect(aggregateQuery, false, null, true /* enable colors */)
      // );

      aggregateQuery.push({
        $skip: Number(skip),
      });

      aggregateQuery.push({
        $limit: Number(maxPerPage),
      });

      let chats = await Chat.aggregate(aggregateQuery);

      if (chats.length > 0) {
        return apiResponse.successResponseWithData(
          res,
          "Operation success",
          chats
        );
      } else {
        return apiResponse.successResponseWithData(
          res,
          "Operation success",
          []
        );
      }
    } catch (err) {
      console.log(err);
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Chat Detail.
 *
 * @param {string}      id
 *
 * @returns {Object}
 */
exports.chatDetail = [
  authBypass,
  async (req, res) => {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return apiResponse.successResponseWithData(res, "Operation success", {});
    }
    try {
      let chat = await Chat.findById(req.params.id)
        .populate({
          path: "messages",
          populate: {
            path: "image",
            model: "Upload",
          },
        })
        .populate({
          path: "messages",
          populate: {
            path: "fake_user",
            populate: {
              path: "image",
              model: "Upload",
            },
          },
        })
        .populate({
          path: "fake_users",
          populate: {
            path: "image",
            model: "Upload",
          },
        })
        .populate({
          path: "chat_image",
          populate: {
            path: "image",
            model: "Upload",
          },
        })
        .populate({
          path: "tags",
        });

      console.log(chat);

      if (chat.is_private && chat.user.toString() != `${req.user._id}`) {
        return apiResponse.unauthorizedResponse(res, "No access");
      }

      let chatViewItem = await ChatView.findOne({
        ip_address: req.clientIp,
        chat: req.params.id,
        updated_by: req.user && req.user._id ? req.user._id : "",
      });

      if (!chatViewItem) {
        let newChatView = new ChatView({
          chat: req.params.id,
          ip_address: req.clientIp,
          updated_by: req.user && req.user._id ? req.user._id : "",
        });

        newChatView.save(function (err) {
          if (err) {
            return apiResponse.ErrorResponse(res, err);
          }
        });
      }

      return apiResponse.successResponseWithData(
        res,
        `Chat ${req.params.id} Success.`,
        chat
      );
    } catch (err) {
      console.log(err);
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Puppeteer Chat Detail.
 *
 * @param {string}      id
 *
 * @returns {Object}
 */
exports.puppeteerChatDetailAccess = [
  async (req, res) => {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return apiResponse.successResponseWithData(res, "Operation success", {});
    }
    try {
      if (req.query.puppeteer_access == process.env.PUPPETEER_SECRET) {
        Chat.findById(req.params.id)
          .populate({
            path: "messages",
            populate: {
              path: "image",
              model: "Upload",
            },
          })
          .populate({
            path: "messages",
            populate: {
              path: "fake_user",
              populate: {
                path: "image",
                model: "Upload",
              },
            },
          })
          .populate({
            path: "fake_users",
            populate: {
              path: "image",
              model: "Upload",
            },
          })
          .populate({
            path: "chat_image",
            populate: {
              path: "image",
              model: "Upload",
            },
          })
          .populate({
            path: "tags",
          })
          .then((chat, err) => {
            if (err) {
              console.log(err);
              return apiResponse.ErrorResponse(res, err);
            }

            console.log(chat);

            return apiResponse.successResponseWithData(
              res,
              `Chat ${req.params.id} Success.`,
              chat
            );
          });
      } else {
        return apiResponse.ErrorResponse(res, "No Access");
      }
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Chat store.
 *
 * @param {string}      title
 * @param {string}      description
 * @param {string}      isbn
 *
 * @returns {Object}
 */

exports.chatStore = [
  auth,
  body("title")
    .custom((value, { req }) => {
      if (req.body.chat_type == 1 && !req.body.title) {
        throw new Error("Title is required");
      }
      return true;
    })
    .trim(),
  body("messages", "Must have at least one message.").not().isEmpty(),
  async (req, res) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      } else {
        var newChat = new Chat({
          title: req.body.title,
          user: req.user._id,
          chat_type: req.body.chat_type,
          description: req.body.description,
          is_private: req.body.is_private,
          chat_image: req.body.chat_image ? req.body.chat_image.id : null,
          chat_layout_type: req.body.chat_layout_type,
        });

        //Save chat.
        newChat = await newChat.save();

        if (req.body.fake_users) {
          let fakeUsers = [];
          let i = 0;
          for (const fakeUser of req.body.fake_users) {
            let newFakeUser = new FakeUser({
              name: fakeUser.name ? fakeUser.name : `User ${i}`,
              image:
                fakeUser.image && fakeUser.image.id ? fakeUser.image.id : null,
              temp_uuid: fakeUser.temp_uuid,
              description: fakeUser.description,
              user: req.user._id,
            });
            i++;
            newFakeUser = await newFakeUser.save();
            fakeUsers.push(newFakeUser);
          }

          newChat.fake_users = fakeUsers;
          newChat = await newChat.save();
        }

        let i = 1;
        if (req.body.messages) {
          let messages = [];
          for (const message of req.body.messages) {
            if (message.message || message.image) {
              let fakeUser = null;
              if (message.fake_user && message.fake_user.temp_uuid) {
                fakeUser = await FakeUser.findOne({
                  temp_uuid: message.fake_user.temp_uuid,
                });
              }
              // FILTERING CURSE WORDS
              if (message.message) {
                message.message = filter.clean(message.message);
              }

              let newMessage = new Message({
                message: message.message,
                image:
                  message.image && message.image.id ? message.image.id : null,
                fake_user: fakeUser ? fakeUser._id : null,
                order: i,
                datetime: message.datetime,
                chat: newChat.id,
                message_type: message.message_type,
              });

              newMessage = await newMessage.save();
              messages.push(newMessage);
            }

            i++;
          }

          newChat.messages = messages;
          newChat = await newChat.save();
        }

        if (req.body.tags) {
          let tags = [];
          for (const tag of req.body.tags) {
            tags.push(tag);
          }

          newChat.tags = tags;
          newChat = await newChat.save();
        }

        let chat = await Chat.findById(newChat._id)
          .populate({
            path: "messages",
            populate: {
              path: "image",
              model: "Upload",
            },
          })
          .populate({
            path: "messages",
            populate: {
              path: "fake_user",
              populate: {
                path: "image",
                model: "Upload",
              },
            },
          })
          .populate({
            path: "fake_users",
            populate: {
              path: "image",
              model: "Upload",
            },
          })
          .populate({
            path: "chat_image",
            populate: {
              path: "image",
              model: "Upload",
            },
          });

        try {
          puppeteer.run(chat);
        } catch (error) {}

        return apiResponse.successResponseWithData(
          res,
          "Chat add Success.",
          chat
        );
      }
    } catch (err) {
      console.log(err);
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Chat update.
 *
 * @param {string}      title
 * @param {string}      description
 * @param {string}      isbn
 *
 * @returns {Object}
 */
exports.chatUpdate = [
  auth,
  body("title")
    .custom((value, { req }) => {
      if (req.body.chat_type == 1 && !value) {
        throw new Error("Title is required");
      }
      return true;
    })
    .trim(),
  body("messages", "Must have at least one message.").not().isEmpty(),

  async (req, res) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      } else {
        if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
          return apiResponse.validationErrorWithData(
            res,
            "Invalid Error.",
            "Invalid ID"
          );
        } else {
          let updatedChat = await Chat.findById(req.params.id);
          if (updatedChat === null) {
            return apiResponse.notFoundResponse(
              res,
              "Chat not exists with this id"
            );
          } else {
            //Check authorized user
            if (updatedChat.user.toString() !== req.user._id) {
              return apiResponse.unauthorizedResponse(
                res,
                "You are not authorized to do this operation."
              );
            } else {
              //update book.
              updatedChat = await Chat.findByIdAndUpdate(
                req.params.id,
                {
                  title: req.body.title,
                  description: req.body.description,
                  is_private: req.body.is_private,
                  chat_image: req.body.chat_image
                    ? req.body.chat_image.id
                    : null,
                  chat_layout_type: req.body.chat_layout_type,
                },
                { new: true }
              );

              // Delete all messages
              await Message.deleteMany({
                chat: req.params.id,
              });
              // updatedChat.messages = [];
              await updatedChat.save();

              if (req.body.fake_users) {
                let fakeUsers = [];
                let i = 0;
                for (const fakeUser of req.body.fake_users) {
                  if (fakeUser && fakeUser._id) {
                    updatedFakeUser = await FakeUser.findByIdAndUpdate(
                      fakeUser._id,
                      {
                        name: fakeUser.name ? fakeUser.name : `User ${i}`,
                        image:
                          fakeUser.image && fakeUser.image.id
                            ? fakeUser.image.id
                            : null,
                        temp_uuid: fakeUser.temp_uuid,
                        description: fakeUser.description,
                        user: req.user._id,
                      },
                      { upsert: true, new: true }
                    );

                    fakeUsers.push(updatedFakeUser._id);
                  } else {
                    let newFakeUser = new FakeUser({
                      name: fakeUser.name ? fakeUser.name : `User ${$i}`,
                      image:
                        fakeUser.image && fakeUser.image._id
                          ? fakeUser.image._id
                          : null,
                      description: fakeUser.description,
                      temp_uuid: fakeUser.temp_uuid,
                      user: req.user._id,
                    });

                    newFakeUser = await newFakeUser.save();
                    fakeUsers.push(newFakeUser.id);
                  }
                  i++;
                }

                updatedChat.fake_users = fakeUsers;
                updatedChat = await updatedChat.save();
              }

              if (req.body.messages) {
                let i = 1;
                let messages = [];
                for (const message of req.body.messages) {
                  if (message.message || message.image) {
                    let fakeUser = null;
                    if (message.fake_user && message.fake_user.temp_uuid) {
                      fakeUser = await FakeUser.findOne({
                        temp_uuid: message.fake_user.temp_uuid,
                      });
                    }

                    let newMessage = new Message({
                      message: message.message,
                      image: message.image ? message.image.id : null,
                      fake_user: fakeUser ? fakeUser._id : null,
                      order: i,
                      datetime: message.datetime,
                      chat: updatedChat.id,
                      message_type: message.message_type,
                    });

                    newMessage = await newMessage.save();
                    messages.push(newMessage);

                    i++;
                  }
                }

                updatedChat.messages = messages;
                updatedChat = await updatedChat.save();
              }

              if (req.body.tags) {
                let tags = [];
                for (const tag of req.body.tags) {
                  tags.push(tag);
                }

                updatedChat.tags = tags;
                updatedChat = await updatedChat.save();
              }

              chat = await Chat.findById(updatedChat._id)
                .populate({
                  path: "messages",
                  populate: {
                    path: "image",
                    model: "Upload",
                  },
                })
                .populate({
                  path: "messages",
                  populate: {
                    path: "fake_user",
                    populate: {
                      path: "image",
                      model: "Upload",
                    },
                  },
                })
                .populate({
                  path: "fake_users",
                  populate: {
                    path: "image",
                    model: "Upload",
                  },
                })
                .populate({
                  path: "chat_image",
                  populate: {
                    path: "image",
                    model: "Upload",
                  },
                })
                .populate({
                  path: "tags",
                });

              puppeteer.run(chat);

              return apiResponse.successResponseWithData(
                res,
                "Chat update Success.",
                chat
              );
            }
          }
        }
      }
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Book Delete.
 *
 * @param {string}      id
 *
 * @returns {Object}
 */
exports.chatDelete = [
  auth,
  async (req, res) => {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return apiResponse.validationErrorWithData(
        res,
        "Invalid Error.",
        "Invalid ID"
      );
    }
    try {
      let foundChat = await Chat.findById(req.params.id);
      if (foundChat === null) {
        return apiResponse.notFoundResponse(
          res,
          "Chat not exists with this id"
        );
      } else {
        //Check authorized user
        if (foundChat.user.toString() !== req.user._id) {
          return apiResponse.unauthorizedResponse(
            res,
            "You are not authorized to do this operation."
          );
        } else {
          //delete book.
          let user = await User.findById(req.user._id);
          user.created_chats = user.created_chats.filter((createdChat) => {
            return createdChat != req.params.id;
          });
          await user.save();

          await Chat.findByIdAndRemove(req.params.id);

          return apiResponse.successResponse(res, "Chat delete success.");
        }
      }
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];
