const UserChatLike = require("../models/UserChatLikeModel");
const Chat = require("../models/ChatModel");
const { body, validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
var mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);

// User Chat Like Schema
function UserChatLikeData(data) {
  this.id = data._id;
  this.chat = data.chat;
  this.user = data.user;
  this.is_liked = data.is_liked;
}

/**
 * User Chat Like List.
 *
 * @returns {Object}
 */
exports.userChatLikeList = [
  auth,
  function (req, res) {
    try {
      let maxPerPage = req.query.max_per_page
        ? Number(req.query.max_per_page)
        : null;
      UserChatLike.find({ user: req.user._id })
        .limit(maxPerPage)
        .then((userChatLike) => {
          if (userChatLike.length > 0) {
            let chatIdItems = [];
            userChatLike.forEach((chatLike) => {
              chatIdItems.push(chatLike.chat);
            });

            Chat.find(
              {
                _id: { $in: chatIdItems },
              },
              "_id title description fake_users messages user chat_type"
            ).then((chats) => {
              return apiResponse.successResponseWithData(
                res,
                "Operation success",
                chats
              );
            });
          } else {
            return apiResponse.successResponseWithData(
              res,
              "Operation success",
              []
            );
          }
        });
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

exports.userChatLikeUpdate = [
  auth,
  body("is_liked", "is_liked is required").not().isEmpty(),
  (req, res) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      } else {
        if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
          return apiResponse.validationErrorWithData(
            res,
            "Invalid Error.",
            "Invalid chat id"
          );
        } else {
          UserChatLike.findOne(
            { user: req.user._id, chat: req.params.id },
            function (err, foundUserChatLike) {
              if (foundUserChatLike === null) {
                var userChatLike = new UserChatLike({
                  user: req.user._id,
                  chat: req.params.id,
                  is_liked: req.body.is_liked,
                });

                userChatLike.save((err) => {
                  if (err) {
                    return apiResponse.ErrorResponse(res, err);
                  }
                });
                let userChatLikeData = new UserChatLikeData(userChatLike);
                return apiResponse.successResponseWithData(
                  res,
                  "Chat liked",
                  userChatLikeData
                );
              } else {
                //Check authorized user
                if (foundUserChatLike.user.toString() !== req.user._id) {
                  return apiResponse.unauthorizedResponse(
                    res,
                    "You are not authorized to do this operation."
                  );
                } else {
                  //update book.
                  UserChatLike.findOneAndUpdate(
                    { user: req.user._id, chat: req.params.id },
                    { is_liked: req.body.is_liked },
                    { new: true },
                    function (err, updatedUserChatLike) {
                      if (err) {
                        return apiResponse.ErrorResponse(res, err);
                      }
                      console.log(updatedUserChatLike);
                      let userChatLikeData = new UserChatLikeData(
                        updatedUserChatLike
                      );
                      return apiResponse.successResponseWithData(
                        res,
                        "User chat like update success.",
                        userChatLikeData
                      );
                    }
                  );
                }
              }
            }
          );
        }
      }
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];
