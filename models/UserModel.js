var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var UserSchema = new mongoose.Schema(
  {
    username: { type: String, required: true },
    first_name: { type: String, required: false },
    last_name: { type: String, required: false },
    email: { type: String, required: true },
    image_url: { type: String },
    google_id: { type: String },
    fb_id: { type: String },
    password: { type: String, required: false },
    is_confirmed: { type: Boolean, required: true, default: 0 },
    confirm_otp: { type: String, required: false },
    otp_tries: { type: Number, required: false, default: 0 },
    status: { type: Boolean, required: true, default: 1 },
    created_chats: [{ type: Schema.Types.ObjectId, ref: "Chat" }],
    // favorites: [{ type: Schema.Types.ObjectId, ref: "Chat" }],
    fake_users: [{ type: Schema.Types.ObjectId, ref: "FakeUser" }],
    updated_at: Date,
    created_at: Date,
  },
  { timestamps: true }
);

// UserSchema.methods.gravatar = function gravatar(size) {
//   if (!size) {
//     size = 200;
//   }
//   if (!this.email) {
//     return `https://gravatar.com/avatar/?s=${size}&d=retro`;
//   }
//   const md5 = crypto.createHash("md5").update(this.email).digest("hex");
//   return `https://gravatar.com/avatar/${md5}?s=${size}&d=retro`;
// };

// Virtual for user's full name
UserSchema.virtual("fullName").get(function () {
  return this.firstName + " " + this.lastName;
});

module.exports = mongoose.model("User", UserSchema);
