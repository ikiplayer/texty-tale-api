const mongoose = require("mongoose");
var Schema = mongoose.Schema;

// USER CAN SEE ALL THEIR CREATED CHATS

const ChatFakeUserSchema = new mongoose.Schema(
  {
    fake_user: { type: Schema.Types.ObjectId, ref: "FakeUser", required: true },
    chat: { type: Schema.Types.ObjectId, ref: "Chat", required: true },
    updated_at: { type: Date },
    created_at: { type: Date },
    updated_by: { type: String },
    created_by: { type: String },
  },
  { timestamps: true }
);

const Chat = mongoose.model("ChatFakeUser", ChatFakeUserSchema);

module.exports = Chat;
