const multer = require("multer");
const path = require("path");
const crypto = require("crypto");
const { v4: uuidv4 } = require("uuid");

/** Storage Engine */
const storageEngine = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, process.env.PWD + "/public/uploads/");
  },
  filename: function (req, file, fn) {
    let customFileName = uuidv4();
    let filename = Date.now() + "_" + customFileName + path.extname(file.originalname);
    fn(null, filename);
  },
});

var validateFile = function (file, cb) {
  allowedFileTypes = /jpeg|jpg|png|gif/;
  const extension = allowedFileTypes.test(path.extname(file.originalname).toLowerCase());
  const mimeType = allowedFileTypes.test(file.mimetype);
  if (extension && mimeType) {
    return cb(null, true);
  } else {
    cb("Invalid file type. Only JPEG, PNG and GIF file are allowed.");
  }
};

//init
const upload = multer({
  storage: storageEngine,
  // limits: { fileSize: 200000 },
  fileFilter: function (req, file, callback) {
    validateFile(file, callback);
  },
}).single("file");

module.exports = upload;
