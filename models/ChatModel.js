const mongoose = require("mongoose");
var Schema = mongoose.Schema;
// USER CAN SEE ALL THEIR CREATED CHATS

const ChatSchema = new mongoose.Schema(
  {
    main_image: { type: Schema.Types.ObjectId, ref: "Upload" },
    chat_image: { type: Schema.Types.ObjectId, ref: "Upload" },
    title: { type: String, required: false },
    description: { type: String, required: false },
    fake_users: [{ type: Schema.Types.ObjectId, ref: "FakeUser" }],
    messages: [{ type: Schema.Types.ObjectId, ref: "Message" }],
    tags: [{ type: Schema.Types.ObjectId, ref: "Tag" }],
    user: { type: Schema.Types.ObjectId, ref: "User", required: true },
    chat_type: { type: Number, default: 0 },
    chat_layout_type: { type: Number, default: 0 },
    views: [{ type: Schema.Types.ObjectId, ref: "ChatView" }],
    likes: [{ type: Schema.Types.ObjectId, ref: "ChatLike" }],
    is_private: { type: Boolean, required: true, default: false },
    status: { type: Number, default: 0 },
    updated_by: { type: Schema.Types.ObjectId, ref: "User" },
    created_by: { type: Schema.Types.ObjectId, ref: "User" },
  },
  { timestamps: true }
);

ChatSchema.index({
  title: "text",
  description: "text",
});


const Chat = mongoose.model("Chat", ChatSchema);

module.exports = Chat;
