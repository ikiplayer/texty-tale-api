const mongoose = require("mongoose");
var Schema = mongoose.Schema;

const FakeUserSchema = new mongoose.Schema(
  {
    name: { type: String },
    image: { type: Schema.Types.ObjectId, ref: "Upload" },
    user: { type: Schema.Types.ObjectId, ref: "User" },
    description: { type: String },
    temp_uuid: { type: String },
    updated_at: { type: Date },
    created_at: { type: Date },
    updated_by: { type: String },
    created_by: { type: String },
  },
  { timestamps: true }
);

const FakeUser = mongoose.model("FakeUser", FakeUserSchema);

module.exports = FakeUser;
