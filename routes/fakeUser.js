var express = require("express");
const FakeUserController = require("../controllers/FakeUserController");

var router = express.Router();

router.get("/", FakeUserController.fakeUserListByUser);
router.post("/", FakeUserController.fakeUserStore);
router.put("/:id", FakeUserController.fakeUserUpdate);
router.delete("/:id", FakeUserController.fakeUserDelete);

module.exports = router;
