const uploadFunction = require("../helpers/upload");
const apiResponse = require("../helpers/apiResponse");
const path = require("path");
const Upload = require("../models/UploadModel");
const { body } = require("express-validator");
const auth = require("../middlewares/jwt");

function UploadData(req, data) {
  this.id = data._id;
  this.filename = data.filename;
  this.mime_type = data.mime_type;
  this.original_name = data.original_name;
  this.type = data.type;
  this.user = data.user;
  this.is_used = data.is_used;
  this.url = `${req.protocol}://${req.headers.host}/uploads/${data.filename}`;
}

exports.uploadFileSave = [
  auth,
  uploadFunction,
  (req, res) => {
    try {
      if (req.file == undefined) {
        return apiResponse.validationErrorWithData(res, "No file found");
      }

      if (!req.body.type == undefined) {
        return apiResponse.validationErrorWithData(res, "Type is required");
      }

      var upload = new Upload({
        filename: req.file.filename,
        mime_type: req.file.mimetype,
        original_name: req.file.originalname,
        size: req.file.size,
        type: req.body.type,
        is_used: req.body.is_used ? req.body.is_used : false,
        user: req.user._id,
      });

      upload.save(function (err) {
        if (err) {
          return apiResponse.ErrorResponse(res, err);
        }
        let uploadData = new UploadData(req, upload);
        return apiResponse.successResponseWithData(res, "Upload Image Add Success.", uploadData);
      });
    } catch (err) {
      console.log(err);
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

exports.getImage = [(res, req) => {}];
