const FakeUser = require("../models/FakeUserModel");
const User = require("../models/UserModel");
const { body, validationResult } = require("express-validator");
const { sanitizeBody } = require("express-validator");
const apiResponse = require("../helpers/apiResponse");
const auth = require("../middlewares/jwt");
var mongoose = require("mongoose");
mongoose.set("useFindAndModify", false);

/**
 * Get Fake Users List by User.
 *
 * @returns {Object}
 */
exports.fakeUserListByUser = [
  auth,
  async (req, res) => {
    try {
      let fakeUsers = await FakeUser.find(
        { user: req.user._id },
        "_id name image_url user"
      );

      if (fakeUsers.length > 0){
        return apiResponse.successResponseWithData(
          res,
          "Operation success",
          fakeUsers
        );
      } else {
        return apiResponse.successResponseWithData(
          res,
          "Operation success",
          []
        );
      }
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * A User can create a fake user
 * Fake user store.
 *
 * @param {string}      name
 * @param {string}      image_url
 *
 * @returns {Object}
 */
exports.fakeUserStore = [
  auth,
  body("name", "Name must not be empty.").isLength({ min: 1 }).trim(),
  async (req, res) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      } else {
        let newFakeUser = new FakeUser(req.body);

        let user = await User.findById(req.user._id);
        user.fake_users.push(newFakeUser);
        await user.save();

        newFakeUser.user = req.user._id;
        newFakeUser = await newFakeUser.save();
        return apiResponse.successResponseWithData(
          res,
          "Chat user add success.",
          newFakeUser
        );
      }
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

exports.fakeUserUpdate = [
  auth,
  body("name", "Name must not be empty.").isLength({ min: 1 }).trim(),
  sanitizeBody("*").escape(),
  async (req, res) => {
    try {
      const errors = validationResult(req);

      if (!errors.isEmpty()) {
        return apiResponse.validationErrorWithData(
          res,
          "Validation Error.",
          errors.array()
        );
      } else {
        if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
          return apiResponse.validationErrorWithData(
            res,
            "Invalid Error.",
            "Invalid ID"
          );
        } else {
          // var updatedFakeUser = new FakeUser(req.body);

          console.log(req.params.id);

          let foundFakeUser = await FakeUser.findById(req.params.id, {
            _id: 0,
          });

          if (foundFakeUser === null) {
            return apiResponse.notFoundResponse(
              res,
              "Chat user does not exists with this id"
            );
          } else {
            //Check authorized user
            if (foundFakeUser.user.toString() !== req.user._id) {
              return apiResponse.unauthorizedResponse(
                res,
                "You are not authorized to do this operation."
              );
            } else {
              //USER FAKE USER

              var updatedFakeUser = await FakeUser.findByIdAndUpdate(
                req.params.id,
                req.body,
                { upsert: true, new: true }
              );

              return apiResponse.successResponseWithData(
                res,
                "Chat user update success.",
                updatedFakeUser
              );
            }
          }
        }
      }
    } catch (err) {
      return apiResponse.ErrorResponse(res, err);
    }
  },
];

/**
 * Fake User Delete.
 *
 * @param {string}      id
 *
 * @returns {Object}
 */
exports.fakeUserDelete = [
  auth,
  async (req, res) => {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      return apiResponse.validationErrorWithData(
        res,
        "Invalid Error.",
        "Invalid ID"
      );
    }
    try {
      let foundFakeUser = await FakeUser.findById(req.params.id);
      if (foundFakeUser === null) {
        return apiResponse.notFoundResponse(
          res,
          "Chat user not exists with this id"
        );
      } else {
        //Check authorized user
        if (foundFakeUser.user.toString() !== req.user._id) {
          return apiResponse.unauthorizedResponse(
            res,
            "You are not authorized to do this operation."
          );
        } else {
          //delete fake user.

          let user = await User.findById(req.user._id);
          user.fake_users = user.fake_users.filter((fakeUser) => {
            return fakeUser != req.params.id;
          });
          await user.save();

          await FakeUser.findByIdAndRemove(req.params.id);
          return apiResponse.successResponse(res, "Chat user delete success.");
        }
      }
    } catch (err) {
      //throw error in json response with status 500.
      return apiResponse.ErrorResponse(res, err);
    }
  },
];
